import axios from "axios";

interface DescriptionObject {
    description: string;
    language: {[name: string]: string};
}

const baseUrl = "https://pokeapi.co/api/v2/characteristic/";

export const fetchPokemonDescription = async (pokemonId: string) => {
    try {
        const { data: {descriptions}} = await axios.get(baseUrl + pokemonId + '/');
        return getEnglishDescription(descriptions);
    } catch(e) {
        console.log(e);
        return "No description provided in PokeAPI";
    }
}

const getEnglishDescription = (descriptions: DescriptionObject[]): string => {
    const descriptionInEnglish = descriptions.find(element => element.language.name == "en");
    if (descriptionInEnglish) {
        return descriptionInEnglish.description;
    } else {
        return "Could not provide pokemon description :(";
    }
};
