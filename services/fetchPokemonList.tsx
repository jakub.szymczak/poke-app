import axios from "axios";
import { Pokemon } from "../domain/Pokemon";

interface FetchProps {
    offset: number;
    limit: number;
};

const baseUrl = "https://pokeapi.co/api/v2/pokemon/";

export const fetchPokemons = async ({offset, limit}: FetchProps) => {
    let pokemonArray: Pokemon[] = [];
    const { data: { results: fetchResult} } = await axios.get(baseUrl + `?limit=${limit}&offset=${offset}`);
    for (const { url: pokemonUrl } of fetchResult) {
        const { data: { id, name, sprites, types } } = await axios.get(pokemonUrl);
        pokemonArray.push(new Pokemon(name, id, sprites.front_default, getPokemonTypes(types)));
    }
    return pokemonArray;
};

const getPokemonTypes = (types: [{ [key: string]: {[key: string]: string} }]) => {
    let typesArray: string[] = [];
    types.forEach((element) => {
        const name = element.type.name;
        typesArray.push(name.charAt(0).toUpperCase() + name.slice(1))
    });
    return typesArray;
};
