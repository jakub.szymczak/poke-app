import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { Pokemon } from './domain/Pokemon';
import { PokemonList } from './components/PokemonList/PokemonList';
import { useEffect, useState } from 'react';
import { fetchPokemons } from './services/fetchPokemonList';
import React  from 'react';


export default function App() {
  const [pokemonArray, setPokemonArray] = useState<Pokemon[]>([]);
  const [pokemonOffset, setPokemonOffset] = useState(0);

  async function getData(offset: number) {
    const pokemons =  await fetchPokemons({offset: offset, limit: 30});
    setPokemonOffset(pokemonOffset + 30);
    setPokemonArray(oldArray => [...oldArray, ...pokemons]);
  }

  useEffect(() => {
    getData(pokemonOffset);
  }, []);

  return (
    <View style={styles.container}>
      {pokemonArray.length > 0 
        ? <PokemonList pokemonList={pokemonArray} onEndReached={()=> {getData(pokemonOffset)}}></PokemonList>
        : <Text>Loading...</Text>}
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    marginTop: 12,
    padding: 8,
  },
});
