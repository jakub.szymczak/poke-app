import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";
import { Pokemon } from "../../domain/Pokemon";
import { POKEMON_TYPE_COLORS } from "../../constants/PokemonColorTypes";
import { PokemonSpecs } from "./PokemonSpecs";
import React  from 'react';


type PokemonProps = {
    pokemon: Pokemon;
    onPokemonTileTouch: () => void;
};

function PokemonTile({ pokemon, onPokemonTileTouch }: PokemonProps) {
    return (
       <TouchableOpacity style={[styles.mainContainer, {backgroundColor: POKEMON_TYPE_COLORS[pokemon.types[0]] }]}
       onPress={onPokemonTileTouch}>
            <Text style={styles.text}>{pokemon.name}</Text>
            <View style={styles.subContainer}>
                <PokemonSpecs specs={pokemon.types}/>
                {pokemon.img && <Image style={styles.image} source={{uri: pokemon.img}}/>}
            </View>     
       </TouchableOpacity>
    );
}

export {PokemonTile};

const styles = StyleSheet.create({
    image: {
        width: 100,
        height: 100,
        paddingRight: 10,
        resizeMode: "contain"
    },
    text: {
        paddingTop: 10,
        alignSelf: "center",
        color: "white",
        fontSize: 15,
        fontWeight: "bold",
        resizeMode: 'contain'
    },
    mainContainer: {
        flex: 1,
        backgroundColor: 'red',
        borderRadius: 10,
        margin: 4    
    },
    subContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    }
})