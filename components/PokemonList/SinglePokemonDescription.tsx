import { FlatList, Modal, View, Text, StyleSheet, TouchableOpacity } from "react-native";
import React, { useEffect, useState }  from 'react';
import { Pokemon } from "../../domain/Pokemon";
import { fetchPokemonDescription } from "../../services/fetchPokemonDescription";
import { POKEMON_TYPE_COLORS } from "../../constants/PokemonColorTypes";

interface SinglePokemonDescriptionProps {
    pokemon: Pokemon;
} 

export const SinglePokemonDescription = ({pokemon}: SinglePokemonDescriptionProps) => {
    const [description, setDescription] = useState("");

    async function fetchDescription() {
        const description = await fetchPokemonDescription(pokemon.id.toString());
        setDescription(description);
        pokemon.description = description;
        console.log(pokemon.description);
    }
    useEffect(() => {
        if (!pokemon.description) {
            fetchDescription()
        } else {
            setDescription(pokemon.description);
        }
    }, []);

    return (
        <View style={ styles.descriptionContainer }>
            <Text style={styles.headerText}>{pokemon.name}</Text>
            <Text style={styles.descriptionText}>{description}</Text>
            <TouchableOpacity>
                <Text style={[styles.button, {backgroundColor: POKEMON_TYPE_COLORS[pokemon.types[0]]}]}>Add to favourites</Text>
            </TouchableOpacity>
        </View>
    )
};

const styles = StyleSheet.create({
    descriptionContainer: {
        flex: 1,
        backgroundColor: 'rgb(237, 233, 232)',
        borderRadius: 16,
        alignItems: 'center',
    },
    headerText: {
        fontSize: 32,
        fontWeight: 'bold',
        margin: 16,
    },
    descriptionText: {
        fontSize: 16,
    },
    button: {
        marginTop: 16,
        padding: 12,
        fontSize: 16,
        fontWeight: 'bold',
        borderRadius: 16,
        overflow: 'hidden',
    }
});
