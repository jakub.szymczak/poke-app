import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";
import { Pokemon } from "../../domain/Pokemon";
import React from "react";
import { POKEMON_TYPE_COLORS } from "../../constants/PokemonColorTypes";
import { SinglePokemonDescription } from "./SinglePokemonDescription";

interface PokemonModalProps {
    pokemon: Pokemon;
}

export const SinglePokemonModal = ({pokemon}: PokemonModalProps) => {
    return (
        <View style={styles.modalOverlay}>
            <View style={[styles.modalContainer, {backgroundColor: POKEMON_TYPE_COLORS[pokemon.types[0]] }]}>
                <Image style={styles.image} source={{uri: pokemon.img}}/>
                <SinglePokemonDescription pokemon={pokemon}/>
            </View>
        </View>
    );
    
};

const styles = StyleSheet.create({
    modalOverlay: {
        backgroundColor: 'rgba(0,0,0,0.5)',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    modalContainer: {
        backgroundColor: 'white',
        width: '80%',
        margin: 16,
        height: '60%',
        borderRadius: 16,
    },
    image: {
       flex: 1,
       resizeMode: 'contain'
    }
})