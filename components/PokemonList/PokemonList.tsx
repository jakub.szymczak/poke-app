import { FlatList, Modal, View, Text, StyleSheet } from "react-native";
import { Pokemon } from "../../domain/Pokemon";
import { PokemonTile } from "./PokemonTile";
import React, { useState }  from 'react';
import { SinglePokemonModal } from "./SinglePokemonModal";

type ListProps = {
    pokemonList: Pokemon[];
    onEndReached: () => void;
}

function PokemonList({ pokemonList, onEndReached }: ListProps) {
    const [isModalVisible, setIsModalVisible] = React.useState(false);
    const [chosenPokemon, setChosenPokemon] = useState<Pokemon>();

    const renderItem = ({ item: pokemon }: {item: Pokemon}) => {
        const pokemonTileTouchedHandler = () => {
            setChosenPokemon(pokemon);
            setIsModalVisible(true);
        };
        return <PokemonTile pokemon={pokemon} onPokemonTileTouch={pokemonTileTouchedHandler}/>
    };

    return (
        <View>
            <Modal
             visible={isModalVisible}
             animationType="fade"
             transparent={true}
             onRequestClose={() => {
                setIsModalVisible(false);
             }}>
                {chosenPokemon && <SinglePokemonModal pokemon={chosenPokemon}></SinglePokemonModal>}
            </Modal>
            <FlatList 
            data={pokemonList}
            renderItem={renderItem}
            keyExtractor={pokemon => pokemon.id.toString()}
            numColumns={2}
            onEndReached={onEndReached}
        />
        </View>
    )
}

const styles = StyleSheet.create({})

export { PokemonList }