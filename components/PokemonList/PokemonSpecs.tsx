import {View, Text, StyleSheet} from 'react-native';
import React  from 'react';

interface SpecProps {
    specs: string[];
}

export function PokemonSpecs({specs}: SpecProps) {
    return (
        <View style={styles.container}>
            {specs.map((spec: string) => {
                return (
                <Text style={styles.text} key={spec}>{spec}</Text>);
            })}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        alignContent: "center",
    },
    text: {
        padding: 4,
        margin: 4,
        color: '#e9ecf0',
        fontWeight: 'bold',
        fontSize: 13,
        borderRadius: 12,
        backgroundColor: 'rgba(0,0,0,0.1)',
        overflow: 'hidden'
    }
})