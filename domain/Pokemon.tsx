class Pokemon {
    public name: string;
    public id: number;
    public img: string;
    public types: string[];
    public description: string | undefined;

    constructor(name: string, id: number, img: string, types: string[]) {
        this.name = name.charAt(0).toUpperCase() + name.slice(1);
        this.id = id;
        this.img = img;
        this.types = types;
    }
}

export { Pokemon }